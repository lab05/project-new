import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DayFitness extends StatefulWidget {
  DayFitness({Key? key}) : super(key: key);

  @override
  _DayFitnessState createState() => _DayFitnessState();
}

class _DayFitnessState extends State<DayFitness> {
  var day = [
    'วันจันทร์',
    'วันอังคาร',
    'วันพุธ',
    'วันพฤหัสบดี',
    'วันศุกร์',
    'วันเสาร์',
    'วันอาทิตย์'
  ];

  var dayValue = [false, false, false, false, false, false, false];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('วันออกกำลังกาย'),
          backgroundColor: Colors.deepPurple.shade300),
      body: Container(
        padding: EdgeInsets.all(20),
        child: ListView(
          children: [
            CheckboxListTile(
              value: dayValue[0],
              title: Text(day[0]),
              onChanged: (newValue) {
                setState(() {
                  dayValue[0] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: dayValue[1],
              title: Text(day[1]),
              onChanged: (newValue) {
                setState(() {
                  dayValue[1] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: dayValue[2],
              title: Text(day[2]),
              onChanged: (newValue) {
                setState(() {
                  dayValue[2] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: dayValue[3],
              title: Text(day[3]),
              onChanged: (newValue) {
                setState(() {
                  dayValue[3] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: dayValue[4],
              title: Text(day[4]),
              onChanged: (newValue) {
                setState(() {
                  dayValue[4] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: dayValue[5],
              title: Text(day[5]),
              onChanged: (newValue) {
                setState(() {
                  dayValue[5] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: dayValue[6],
              title: Text(day[6]),
              onChanged: (newValue) {
                setState(() {
                  dayValue[6] = newValue!;
                });
              },
            ),
            ElevatedButton(
              onPressed: () {
                _saveDayFitness();
                Navigator.pop(context);
              },
              child: const Text('Save'),
              style: ElevatedButton.styleFrom(
                primary: Colors.deepPurple[300], // background
                onPrimary: Colors.white, // foreground
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.deepPurple[50],
    );
  }

  @override
  void initState() {
    super.initState();
    _loadDayFitness();
  }

  Future<void> _loadDayFitness() async {
    var prefs = await SharedPreferences.getInstance();
    var strDayValue = prefs.getString('day_value') ??
        '[false, false, false, false, false, false, false]';
    var arrStrDayValues =
        strDayValue.substring(1, strDayValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrDayValues.length; i++) {
        dayValue[i] = (arrStrDayValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveDayFitness() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('day_value', dayValue.toString());
  }
}
