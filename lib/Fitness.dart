class Fitness {
  int id;
  String nameFit;
  int timeFit;

  Fitness({
    required this.id,
    required this.nameFit,
    required this.timeFit,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nameFit': nameFit,
      'timeFit': timeFit,
    };
  }

  static List<Fitness> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Fitness(
        id: maps[i]['id'],
        nameFit: maps[i]['nameFit'],
        timeFit: maps[i]['timeFit'],
      );
    });
  }

  @override
  String toString() {
    return '{id: $id, nameFit: $nameFit, timeFit: $timeFit}';
  }
}
