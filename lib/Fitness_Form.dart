import 'package:covid_app/Fitness.dart';
import 'package:covid_app/Fitness_Service.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FitnessForm extends StatefulWidget {
  Fitness fit;
  FitnessForm({Key? key, required this.fit}) : super(key: key);

  @override
  _FitnessFormState createState() => _FitnessFormState(fit);
}

class _FitnessFormState extends State<FitnessForm> {
  Fitness fit;
  _FitnessFormState(this.fit);
  final _formKey = GlobalKey<FormState>();
  String fitness1 = '';
  int time = 0;

  @override
  void initState() {
    super.initState();
    _loadFitness();
  }

  _loadFitness() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      fitness1 = prefs.getString('fitness1') ?? '';
      time = prefs.getInt('time') ?? 0;
    });
  }

  _saveFitness() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('fitness1', fitness1);
      prefs.setInt('time', time);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('ประเภทการออกกำลังกาย'),
          backgroundColor: Colors.deepPurple.shade300),
      body: Container(
          padding: EdgeInsets.all(20),
          child: Form(
            key: _formKey,
            child: Container(
              padding: EdgeInsets.all(20),
              child: ListView(
                children: [
                  TextFormField(
                    autofocus: true,
                    initialValue: fit.nameFit,
                    validator: (val) {
                      if (val == null || val.isEmpty || val.length < 2) {
                        return 'กรุณาใส่ประเ๓ทการออกกำลังกาย';
                      }
                      return null;
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      labelText: 'ชื่อการออกกำลังกาย',
                    ),
                    onChanged: (String? val) {
                      fit.nameFit = val!;
                      setState(() {
                        fitness1 = val;
                      });
                    },
                  ),
                  Padding(padding: EdgeInsets.all(10)),
                  TextFormField(
                    initialValue: fit.timeFit.toString(),
                    validator: (val) {
                      if (val == null || val.isEmpty) {
                        return 'กรุณาใส่ข้อมูเวลาการออกกำลังกาย';
                      }
                      return null;
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      labelText: 'เวลาการออกกำลังกาย',
                    ),
                    onChanged: (String? val) {
                      fit.timeFit = int.parse(val!);
                      setState(() {
                        time = int.tryParse(val)!;
                      });
                    },
                    keyboardType: TextInputType.number,
                  ),
                  Padding(padding: EdgeInsets.all(10)),
                  ElevatedButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        if (fit.id > 0) {
                          await saveFit(fit);
                        } else {
                          await addNew(fit);
                        }
                        _saveFitness();
                        Navigator.pop(context);
                      }
                    },
                    child: const Text('บันทึก'),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.deepPurple[300], // background
                      onPrimary: Colors.white, // foreground
                    ),
                  ),
                ],
              ),
            ),
          )),
      backgroundColor: Colors.deepPurple[50],
    );
  }
}
