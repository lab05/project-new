import 'package:covid_app/Fitness.dart';

var lastId = 3;
var mockFits = [
  Fitness(id: 1, nameFit: 'วิ่ง', timeFit: 60),
  Fitness(id: 2, nameFit: 'ฮูล่าฮูป', timeFit: 30)
];

int getNewId() {
  return lastId;
}

Future<void> addNew(Fitness fit) {
  return Future.delayed(Duration(seconds: 0), () {
    mockFits.add(
        Fitness(id: getNewId(), nameFit: fit.nameFit, timeFit: fit.timeFit));
  });
}

Future<void> saveFit(Fitness fit) {
  return Future.delayed(Duration(seconds: 0), () {
    var index = mockFits.indexWhere((item) => item.id == fit.id);
    mockFits[index] = fit;
  });
}

Future<void> delFit(Fitness fit) {
  return Future.delayed(Duration(seconds: 0), () {
    var index = mockFits.indexWhere((item) => item.id == fit.id);
    mockFits.removeAt(index);
  });
}

Future<List<Fitness>> getFits() {
  return Future.delayed(Duration(seconds: 0), () => mockFits);
}
