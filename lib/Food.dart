class Food {
  int id;
  String nameFood;
  int calorie;
  String timeFood;

  Food(
      {required this.id,
      required this.nameFood,
      required this.calorie,
      required this.timeFood});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nameFood': nameFood,
      'calorie': calorie,
      'timeFood': timeFood,
    };
  }

  static List<Food> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Food(
          id: maps[i]['id'],
          nameFood: maps[i]['nameFood'],
          calorie: maps[i]['calorie'],
          timeFood: maps[i]['timeFood']);
    });
  }

  @override
  String toString() {
    return '{id: $id, nameFood : $nameFood, calorie: $calorie, timeFood: $timeFood}';
  }
}
