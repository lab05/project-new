import 'package:covid_app/Food.dart';
import 'package:covid_app/Food_Sevice.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FoodForm extends StatefulWidget {
  Food food;
  FoodForm({Key? key, required this.food}) : super(key: key);

  @override
  _FoodFormState createState() => _FoodFormState(food);
}

class _FoodFormState extends State<FoodForm> {
  Food food;
  _FoodFormState(this.food);
  final _formKey = GlobalKey<FormState>();
  String foodName = '';
  int calorie = 0;
  String foodTime = '-';

  @override
  void initState() {
    super.initState();
    _loadFood();
  }

  Future<void> _loadFood() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      foodName = prefs.getString('foodName') ?? '';
      calorie = prefs.getInt('calorie') ?? 0;
      foodTime = prefs.getString('foodTime') ?? '';
    });
  }

  Future<void> _saveFood() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('foodName', foodName);
      prefs.setInt('calorie', calorie);
      prefs.setString('foodTime', foodTime);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('บันทึกการกิน'),
          backgroundColor: Colors.deepPurple.shade300),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20),
          child: ListView(
            children: [
              TextFormField(
                autofocus: true,
                initialValue: food.nameFood,
                validator: (val) {
                  if (val == null || val.isEmpty || val.length < 2) {
                    return 'กรุณาใส่ชื่ออาหาร';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(
                  labelText: 'ชื่ออาหาร',
                ),
                onChanged: (String? val) {
                  food.nameFood = val!;
                  setState(() {
                    foodName = val;
                  });
                },
              ),
              TextFormField(
                initialValue: food.calorie.toString(),
                validator: (val) {
                  if (val == null || val.isEmpty) {
                    return 'กรุณาใส่ข้อมูลแคลอรี่ของอาหาร';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(
                  labelText: 'จำนวนแคลอรี่',
                ),
                onChanged: (String? val) {
                  food.calorie = int.parse(val!);
                  setState(() {
                    calorie = int.tryParse(val)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                initialValue: food.timeFood,
                validator: (val) {
                  if (val == null || val.isEmpty) {
                    return 'กรุณาใส่มื้ออาหาร';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(
                  labelText: 'มื้ออาหาร',
                ),
                onChanged: (String? val) {
                  food.timeFood = val!;
                  setState(() {
                    foodTime = val;
                  });
                },
              ),
              Padding(padding: EdgeInsets.all(10)),
              ElevatedButton(
                onPressed: () async {
                  if (food.id > 0) {
                    await saveFood(food);
                  } else {
                    await addNew(food);
                  }
                  _saveFood();
                  Navigator.pop(context);
                },
                child: const Text('บันทึก'),
                style: ElevatedButton.styleFrom(
                  primary: Colors.deepPurple[300], // background
                  onPrimary: Colors.white, // foreground
                ),
              ),
            ],
          ),
        ),
      ),
      backgroundColor: Colors.deepPurple[50],
    );
  }
}
