import 'package:covid_app/Food.dart';

var lastId = 3;
var mockFoods = [
  Food(id: 1, nameFood: 'ข้าวมันไก่', calorie: 596, timeFood: 'อาหารกลางวัน'),
  Food(id: 2, nameFood: 'นมไมโล', calorie: 130, timeFood: 'ของว่าง')
];

int getNewId() {
  return lastId;
}

Future<void> addNew(Food food) {
  return Future.delayed(Duration(seconds: 0), () {
    mockFoods.add(Food(
        id: getNewId(),
        nameFood: food.nameFood,
        calorie: food.calorie,
        timeFood: food.timeFood));
  });
}

Future<void> saveFood(Food food) {
  return Future.delayed(Duration(seconds: 0), () {
    var index = mockFoods.indexWhere((item) => item.id == food.id);
    mockFoods[index] = food;
  });
}

Future<void> delFood(Food food) {
  return Future.delayed(Duration(seconds: 0), () {
    var index = mockFoods.indexWhere((item) => item.id == food.id);
    mockFoods.removeAt(index);
  });
}

Future<List<Food>> getFoods() {
  return Future.delayed(Duration(seconds: 0), () => mockFoods);
}
