import 'package:covid_app/Fitness.dart';
import 'package:covid_app/Fitness_Form.dart';
import 'package:covid_app/Fitness_Service.dart';
import 'package:flutter/material.dart';

class HisFitness extends StatefulWidget {
  HisFitness({Key? key}) : super(key: key);

  @override
  _HisFitnessState createState() => _HisFitnessState();
}

class _HisFitnessState extends State<HisFitness> {
  late Future<List<Fitness>> _fits;
  @override
  void initState() {
    super.initState();
    _fits = getFits();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('การออกกำลังกาย'),
        backgroundColor: Colors.deepPurple.shade300,
      ),
      body: FutureBuilder(
        future: _fits,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error');
          }
          List<Fitness> fits = snapshot.data as List<Fitness>;
          return ListView.builder(
            itemBuilder: (context, index) {
              var fit = fits.elementAt(index);
              return ListTile(
                title: Text('บันทึกครั้งที่ ${fit.id}'),
                subtitle: Text(
                    'ประเภทการออกกำลังกาย : ${fit.nameFit} , เวลา : ${fit.timeFit} นาที'),
                trailing: IconButton(
                  icon: Icon(Icons.delete_forever_rounded,
                      color: Colors.deepPurple.shade400),
                  onPressed: () async {
                    await delFit(fit);
                    setState(() {
                      _fits = getFits();
                    });
                  },
                ),
                onTap: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FitnessForm(
                                fit: fit,
                              )));
                  setState(() {
                    _fits = getFits();
                  });
                },
                tileColor: Colors.purple.shade100,
              );
            },
            itemCount: fits.length,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_circle_outline_outlined, color: Colors.white),
        onPressed: () async {
          Fitness newFit = Fitness(id: -1, nameFit: '', timeFit: 0);
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => FitnessForm(
                        fit: newFit,
                      )));
          setState(() {
            _fits = getFits();
          });
        },
        tooltip: 'เพิ่มบันทึกการออกกำลังกาย',
        backgroundColor: Colors.deepPurple[400],
      ),
      backgroundColor: Colors.deepPurple.shade50,
    );
  }
}
