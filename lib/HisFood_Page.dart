import 'package:covid_app/Food.dart';
import 'package:covid_app/Food_Form.dart';
import 'package:covid_app/Food_Sevice.dart';
import 'package:flutter/material.dart';

class HisFood extends StatefulWidget {
  HisFood({Key? key}) : super(key: key);

  @override
  _HisFoodState createState() => _HisFoodState();
}

class _HisFoodState extends State<HisFood> {
  late Future<List<Food>> _foods;
  @override
  void initState() {
    super.initState();
    _foods = getFoods();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('อาหาร'),
        backgroundColor: Colors.deepPurple.shade300,
      ),
      body: FutureBuilder(
        future: _foods,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error');
          }
          List<Food> foods = snapshot.data as List<Food>;
          return ListView.builder(
            itemBuilder: (context, index) {
              var food = foods.elementAt(index);
              return ListTile(
                title: Text(food.nameFood),
                subtitle: Text(
                    'แคลอรี่ : ${food.calorie} Cal , มื้ออาหาร : ${food.timeFood}'),
                trailing: IconButton(
                  icon: Icon(Icons.delete_forever_rounded,
                      color: Colors.deepPurple.shade400),
                  onPressed: () async {
                    await delFood(food);
                    setState(() {
                      _foods = getFoods();
                    });
                  },
                ),
                onTap: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FoodForm(
                                food: food,
                              )));
                  setState(() {
                    _foods = getFoods();
                  });
                },
                tileColor: Colors.purple.shade100,
              );
            },
            itemCount: foods.length,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_circle_outline_outlined, color: Colors.white),
        onPressed: () async {
          Food newFood = Food(id: -1, nameFood: '', calorie: 0, timeFood: '');
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => FoodForm(
                        food: newFood,
                      )));
          setState(() {
            _foods = getFoods();
          });
        },
        tooltip: 'เพิ่มบันทึกการกิน',
        backgroundColor: Colors.deepPurple[400],
      ),
      backgroundColor: Colors.deepPurple.shade50,
    );
  }
}
