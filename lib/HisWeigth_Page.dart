import 'package:covid_app/Weight_Form.dart';
import 'package:covid_app/WeigthHigh.dart';
import 'package:flutter/material.dart';
import 'WeigthHigh.dart';
import 'Weigth_Service.dart';

class HisWeigth extends StatefulWidget {
  HisWeigth({Key? key}) : super(key: key);

  @override
  _HisWeigthState createState() => _HisWeigthState();
}

class _HisWeigthState extends State<HisWeigth> {
  late Future<List<WeigthHigh>> _pops;
  @override
  void initState() {
    super.initState();
    _pops = getWeithHigh();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('น้ำหนัก-ส่วนสูง'),
        backgroundColor: Colors.deepPurple.shade300,
      ),
      body: FutureBuilder(
        future: _pops,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error');
          }
          List<WeigthHigh> pops = snapshot.data as List<WeigthHigh>;
          return ListView.builder(
            itemBuilder: (context, index) {
              var pop = pops.elementAt(index);
              return ListTile(
                title: Text('บันทึกครั้งที่ ${pop.id}'),
                subtitle: Text(
                    'น้ำหนัก : ${pop.weigth} กก. , ส่วนสูง : ${pop.high} ซม.'),
                trailing: IconButton(
                  icon: Icon(Icons.delete_forever_rounded,
                      color: Colors.deepPurple.shade400),
                  onPressed: () async {
                    await delWeigthHigh(pop);
                    setState(() {
                      _pops = getWeithHigh();
                    });
                  },
                ),
                onTap: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => WeightHighForm(pop: pop)));
                  setState(() {
                    _pops = getWeithHigh();
                  });
                },
                tileColor: Colors.purple.shade100,
              );
            },
            itemCount: pops.length,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_circle_outline_outlined, color: Colors.white),
        onPressed: () async {
          WeigthHigh newPop = WeigthHigh(id: -1, weigth: 0, high: 0);
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => WeightHighForm(pop: newPop)));
          setState(() {
            _pops = getWeithHigh();
          });
        },
        tooltip: 'เพิ่มบันทึกน้ำหนัก-ส่วนสูง',
        backgroundColor: Colors.deepPurple[400],
      ),
      backgroundColor: Colors.deepPurple.shade50,
    );
  }
}
