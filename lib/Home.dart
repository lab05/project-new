import 'package:covid_app/DayFitness_Page.dart';
import 'package:covid_app/HisFitness.dart';
import 'package:covid_app/HisFood_Page.dart';
import 'package:covid_app/HisWeigth_Page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int weight = 0;
  int high = 0;
  String fitness1 = '-';
  int time = 0;
  var dayScore = 0.0;
  var kec = 0;
  String foodName = '';
  int calorie = 0;
  String foodTime = '';
  var cal = 0;
  double bmi = 0.0;
  String day = '';
  String textBMI = '';

  @override
  void initState() {
    super.initState();
    _loadWeightHigh();
    _loadFitness();
    _loadDayFitness();
    _loadFood();
  }

  Future<void> _loadFitness() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      fitness1 = prefs.getString('fitness1') ?? '-';
      time = prefs.getInt('time') ?? 0;
      kec = time * 10;
    });
  }

  Future<void> _loadWeightHigh() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      weight = prefs.getInt('weight') ?? 0;
      high = prefs.getInt('high') ?? 0;
      bmi = prefs.getDouble('bmi') ?? 0;
      if (bmi < 18 && bmi != 0) {
        textBMI = 'น้ำหนักน้อยกว่าเกณฑ์';
      } else if (bmi >= 18 && bmi <= 23) {
        textBMI = 'สมส่วน';
      } else if (bmi > 23 && bmi <= 25) {
        textBMI = 'น้ำหนักเกินเกณฑ์';
      } else if (bmi > 25 && bmi <= 30) {
        textBMI = 'เสี่ยงโรคอ้วน';
      } else if (bmi > 30) {
        textBMI = 'โรคอ้วนระดับอันตราย';
      } else {
        textBMI = '';
      }
    });
  }

  Future<void> _loadDayFitness() async {
    var prefs = await SharedPreferences.getInstance();
    var strDayValue = prefs.getString('day_value') ??
        '[false, false, false, false, false, false, false]';
    var arrStrDayValues =
        strDayValue.substring(1, strDayValue.length - 1).split(',');
    setState(() {
      dayScore = 0.0;
      for (var i = 0; i < arrStrDayValues.length; i++) {
        if (arrStrDayValues[i].trim() == 'true') {
          dayScore = dayScore + 1;
        }
      }
      dayScore = (dayScore * 100) / 7;
      if (dayScore < 40 && dayScore != 0) {
        day = 'ไม่สม่ำเสมอ';
      } else if (dayScore > 40) {
        day = 'สม่ำเสมอ';
      } else {
        day = '';
      }
    });
  }

  Future<void> _loadFood() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      foodName = prefs.getString('foodName') ?? '';
      calorie = prefs.getInt('calorie') ?? 0;
      foodTime = prefs.getString('foodTime') ?? '-';
      if (cal == 0) {
        cal = calorie;
      } else {
        cal = cal + calorie;
      }
    });
  }

  Future<void> _resetAll() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('weight');
    prefs.remove('high');
    prefs.remove('bmi');
    prefs.remove('fitness1');
    prefs.remove('time');
    prefs.remove('day_value');
    prefs.remove('foodName');
    prefs.remove('calorie');
    prefs.remove('foodTime');
    cal = 0;
    await _loadWeightHigh();
    await _loadFitness();
    await _loadDayFitness();
    await _loadFood();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'To My Health',
          style: TextStyle(fontSize: 30, color: Colors.white),
        ),
        backgroundColor: Colors.deepPurple.shade300,
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: [
          Card(
            child: Column(
              children: [
                ListTile(
                  leading: Icon(
                    Icons.account_balance_wallet_outlined,
                    color: Colors.deepPurple.shade400,
                  ),
                  title: Text('น้ำหนัก'),
                  subtitle: Text(
                    '$weight kg',
                    style: TextStyle(fontSize: 20, color: Colors.teal),
                  ),
                ),
                Row(
                  children: [
                    Padding(
                        padding: const EdgeInsets.only(left: 15, bottom: 15),
                        child: Text(
                          'ค่า BMI :',
                          style: TextStyle(fontSize: 15, color: Colors.brown),
                        )),
                    Padding(
                        padding: const EdgeInsets.only(left: 15, bottom: 15),
                        child: Text(
                          '${bmi.toStringAsFixed(2)}  $textBMI',
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.normal,
                              color: (bmi >= 18 && bmi <= 23)
                                  ? Colors.green.shade300
                                  : Colors.pink.shade200),
                        )),
                  ],
                )
              ],
            ),
          ),
          Card(
            child: Column(
              children: [
                ListTile(
                  leading: Icon(
                    Icons.food_bank_outlined,
                    color: Colors.deepPurple.shade400,
                  ),
                  title: Text('อาหาร'),
                  subtitle: Text(
                    '$cal แคลอรี่',
                    style: TextStyle(fontSize: 20, color: Colors.teal),
                  ),
                ),
                Row(
                  children: [
                    Padding(
                        padding: const EdgeInsets.all(15),
                        child: Text(
                          'บริมาณแคลอรี่ที่ต้องการต่อวัน 1403 แคลอรี่',
                          style: TextStyle(fontSize: 15, color: Colors.brown),
                        )),
                  ],
                )
              ],
            ),
          ),
          Card(
            child: Column(
              children: [
                ListTile(
                  leading: Icon(
                    Icons.accessibility_new_outlined,
                    color: Colors.deepPurple.shade400,
                  ),
                  title: Text('การออกกำลังกาย'),
                  subtitle: Text(
                    '$time นาที',
                    style: TextStyle(fontSize: 20, color: Colors.teal),
                  ),
                ),
                Row(
                  children: [
                    Padding(
                        padding: const EdgeInsets.only(left: 15, bottom: 15),
                        child: Text(
                          'ประเภทการออกลังกาย :',
                          style: TextStyle(fontSize: 15, color: Colors.brown),
                        )),
                    Padding(
                        padding: const EdgeInsets.only(left: 15, bottom: 15),
                        child: Text(
                          '$fitness1',
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: 20,
                              color: Colors.green.shade300),
                        )),
                  ],
                ),
                Row(
                  children: [
                    Padding(
                        padding: const EdgeInsets.only(left: 15, bottom: 15),
                        child: Text(
                          'จำนวนพลังงานที่เผาผลาญ :',
                          style: TextStyle(fontSize: 15, color: Colors.brown),
                        )),
                    Padding(
                        padding: const EdgeInsets.only(left: 15, bottom: 15),
                        child: Text(
                          '$kec แคลอรี่',
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: 20,
                              color: (kec > 0)
                                  ? Colors.green.shade300
                                  : Colors.pink.shade200),
                        )),
                  ],
                ),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 15, bottom: 15),
                      child: Text(
                        'ความสม่ำเสมอในการออกกำลังกาย :',
                        style: TextStyle(fontSize: 15, color: Colors.brown),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15, bottom: 15),
                      child: Text(
                        '${dayScore.toStringAsFixed(2)} %  $day',
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 20,
                            color: (dayScore > 40)
                                ? Colors.green.shade300
                                : Colors.pink.shade200),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _resetAll,
        child: Icon(
          Icons.restore_outlined,
          color: Colors.white,
        ),
        tooltip: 'Reset',
        backgroundColor: Colors.deepPurple[600],
      ),
      drawer: Drawer(
          child: Container(
        color: Colors.deepPurple.shade100,
        child: ListView(
          children: [
            DrawerHeader(
              child: Text(
                'เมนู',
                style: TextStyle(fontSize: 30, color: Colors.white),
              ),
              decoration: BoxDecoration(color: Colors.deepPurple.shade300),
            ),
            ListTile(
              title: Text('น้ำหนัก-ส่วนสูง'),
              subtitle: Text('บันทึกน้ำหนัก-ส่วนสูง, ประวัติการบันทึก'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HisWeigth()));
                await _loadWeightHigh();
              },
            ),
            ListTile(
              title: Text('อาหาร'),
              subtitle: Text('บันทึกมื้ออาหาร, ประวัติการบันทึก'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HisFood()));
                await _loadFood();
              },
            ),
            ListTile(
              title: Text('การออกกำลังกาย'),
              subtitle: Text('บันการออกกำลังกายและเวลา, ประวัติการบันทึก'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HisFitness()));
                await _loadFitness();
              },
            ),
            ListTile(
              title: Text('ความสม่ำเสมอ'),
              subtitle: Text('บันทึกวันที่ออกกำลังกาย (จ.- อา.)'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DayFitness()));
                await _loadDayFitness();
              },
            ),
          ],
        ),
      )),
      backgroundColor: Colors.deepPurple.shade100,
    );
  }
}
