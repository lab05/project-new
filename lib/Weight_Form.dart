import 'package:covid_app/WeigthHigh.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Weigth_Service.dart';

class WeightHighForm extends StatefulWidget {
  WeigthHigh pop;
  WeightHighForm({Key? key, required this.pop}) : super(key: key);

  @override
  _WeightHighFormState createState() => _WeightHighFormState(pop);
}

class _WeightHighFormState extends State<WeightHighForm> {
  final _formKey = GlobalKey<FormState>();
  WeigthHigh pop;
  _WeightHighFormState(this.pop);
  int weight = 0;
  int high = 0;
  double bmi = 0.0;
  double high2 = 0.0;
  double weight2 = 0.0;

  @override
  void initState() {
    super.initState();
    _loadWeightHigh();
  }

  Future<void> _loadWeightHigh() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      weight = prefs.getInt('weight') ?? 0;
      high = prefs.getInt('high') ?? 0;
      bmi = 0.0;
    });
  }

  Future<void> _seveWeightHigh() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setInt('weight', weight);
      prefs.setInt('high', high);
      bmi = 0.0;
      high2 = (high / 100) * (high / 100);
      weight2 = weight as double;
      bmi = (weight2 / high2);
      prefs.setDouble('bmi', bmi);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('บันทึกน้ำหนัก ส่วนสูง'),
          backgroundColor: Colors.deepPurple.shade300,
        ),
        body: Form(
            key: _formKey,
            child: Container(
              padding: EdgeInsets.all(20),
              child: ListView(
                children: [
                  TextFormField(
                    autofocus: true,
                    initialValue: pop.weigth.toString(),
                    validator: (val) {
                      if (val == null || val.isEmpty) {
                        return 'กรุณาใส่ข้อมูลน้ำหนัก';
                      }
                      return null;
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      labelText: 'น้ำหนัก (กิโลกรัม)',
                    ),
                    onChanged: (String? val) {
                      pop.weigth = int.parse(val!);
                      setState(() {
                        weight = int.tryParse(val)!;
                      });
                    },
                    keyboardType: TextInputType.number,
                  ),
                  Padding(padding: EdgeInsets.all(10)),
                  TextFormField(
                    initialValue: pop.high.toString(),
                    validator: (val) {
                      if (val == null || val.isEmpty) {
                        return 'กรุณาใส่ข้อมูลส่วนสูง';
                      }
                      return null;
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      labelText: 'ส่วนสูง (เซนติเมตร)',
                    ),
                    onChanged: (String? val) {
                      pop.high = int.parse(val!);
                      setState(() {
                        high = int.tryParse(val)!;
                      });
                    },
                    keyboardType: TextInputType.number,
                  ),
                  Padding(padding: EdgeInsets.all(10)),
                  ElevatedButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        if (pop.id > 0) {
                          await saveWeigthHigh(pop);
                        } else {
                          await addNew(pop);
                        }
                        _seveWeightHigh();
                        Navigator.pop(context);
                      }
                    },
                    child: const Text('บันทึก'),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.purple[300], // background
                      onPrimary: Colors.white, // foreground
                    ),
                  )
                ],
              ),
            )));
  }
}
