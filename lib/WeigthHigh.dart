class WeigthHigh {
  int id;
  int weigth;
  int high;

  WeigthHigh({
    required this.id,
    required this.weigth,
    required this.high,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'weigth': weigth,
      'high': high,
    };
  }

  static List<WeigthHigh> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return WeigthHigh(
          id: maps[i]['id'], weigth: maps[i]['weigth'], high: maps[i]['high']);
    });
  }

  String toString() {
    return '{id: $id, weigth: $weigth, high: $high}';
  }
}
