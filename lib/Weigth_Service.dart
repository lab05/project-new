import 'WeigthHigh.dart';

var lastId = 5;
var mockWeigthHigh = [
  WeigthHigh(id: 1, weigth: 45, high: 150),
  WeigthHigh(id: 2, weigth: 47, high: 150),
  WeigthHigh(id: 3, weigth: 70, high: 183),
  WeigthHigh(id: 4, weigth: 50, high: 165)
];

int getNewId() {
  return lastId;
}

Future<void> addNew(WeigthHigh pop) {
  return Future.delayed(Duration(seconds: 0), () {
    mockWeigthHigh
        .add(WeigthHigh(id: getNewId(), weigth: pop.weigth, high: pop.high));
  });
}

Future<void> saveWeigthHigh(WeigthHigh pop) {
  return Future.delayed(Duration(seconds: 0), () {
    var index = mockWeigthHigh.indexWhere((item) => item.id == pop.id);
    mockWeigthHigh[index] = pop;
  });
}

Future<void> delWeigthHigh(WeigthHigh pop) {
  return Future.delayed(Duration(seconds: 0), () {
    var index = mockWeigthHigh.indexWhere((item) => item.id == pop.id);
    mockWeigthHigh.removeAt(index);
  });
}

Future<List<WeigthHigh>> getWeithHigh() {
  return Future.delayed(Duration(seconds: 0), () => mockWeigthHigh);
}
